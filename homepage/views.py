from django.shortcuts import render, redirect
from .models import Review
from . import forms
from django.http import JsonResponse

def review(request):
    reviews = Review.objects.all().order_by('id')
    return render(request, 'input.html', {'reviews': reviews})

def review_create(request):
    reviews = Review.objects.all().order_by('id')
    if request.method == 'POST':
        form = forms.ReviewForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:review_create')

    else:
        form = forms.ReviewForm()
    return render(request, 'input.html', {'form': form, 'reviews':reviews})

def get_JSON_daftar_review(request,value):
    result = list(Review.objects.filter(name=value).values())
    return JsonResponse({"data": result})