from django.db import models


class Review(models.Model):

    fill = models.CharField(max_length=200)
    name = models.CharField(max_length=22, default='anonimous')

    def __str__(self):
        return "{}. {}".format(self.id, self.name)
