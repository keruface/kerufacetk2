from django import forms
from .models import Review


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ['name','fill']
        widgets = {
            'name':forms.TextInput(attrs={'placeholder':'Masukkan nama anda'}),
            'fill':forms.TextInput(attrs={'placeholder':'Masukkan kritik dan saran anda'}),
            }
        labels = { 
            'name':'Nama',
            'fill':'Kritik dan saran',
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
