from django.test import TestCase,Client
from homepage.models import Review
from homepage.forms import ReviewForm
from django.test import TestCase, override_settings

@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
# Create your tests here.
class TestBarangViews (TestCase):

    def test_html(self):
        self.client = Client()
        response = self.client.get('/review/')
        self.assertEqual(response.status_code, 200)

    def test_html_formBarang(self):
        self.client = Client()
        response = self.client.get('/review/')
        self.assertTemplateUsed(response,"input.html")
    
    def test_html_homepage(self):
        self.client = Client()
        response = self.client.get('/review/')
        self.assertTemplateUsed(response,"input.html")

    def test_html_homepage_1(self):
        self.client = Client()
        response = self.client.get('/review/')
        self.assertEqual(response.status_code, 200)
    
    def test_contain_name (self):
        response = Client().get('/review/')
        html = response.content.decode('utf8')
        self.assertIn("Tell",html)

    def test_else_does_not_exist(self):
        response = Client().get('/asasasas/')
        self.assertEqual(response.status_code,404)
    
class TestBarangModel(TestCase):

    def test_buat_barang(self):
        review_save = Review(
            name = "Lola",
            fill = "Bagus banget websitenya",
        )
        review_save.save()  
        self.assertEqual(Review.objects.all().count(), 1)

    def test_buat_data(self):
        data = {
            'name' : "Lola",
            'fill' : "Bagus banget websitenya",
        }
        response = Client().post("/review/", data)
        self.assertEqual(response.status_code, 302)


