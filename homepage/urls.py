from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.review_create, name='review_create'),
    path('daftar_review/<str:value>/' , views.get_JSON_daftar_review , name = "daftar_review")
]