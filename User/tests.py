from django.test import TestCase
import os
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve
from User.views import login_view, logout_view, signup_view

# Create your tests here.
class UserTest(TestCase):
    
    def setUp(self):
        user = User.objects.create_user(username="samlekom",password="alfanganteng")
        user.save()

    def test_url_login_is_exist(self):
        response = self.client.get('/user/login/')
        self.assertEqual(response.status_code,200)

    def test_url_logout_is_exist(self):
        response = self.client.get('/user/logout/')
        self.assertEqual(response.status_code,200)

    def test_url_signup_is_exist(self):
        response = self.client.get('/user/signup/')
        self.assertEqual(response.status_code,200)
    
    def test_url_does_not_exist(self):
        response = self.client.get('/user/wek/')
        self.assertEqual(response.status_code,404)

    def test_using_correct_signin_template(self):
        response = self.client.get('/user/login/')
        self.assertTemplateUsed(response,'User/login.html')

    def test_using_correct_signup_template(self):
        response = self.client.get('/user/signup/')
        self.assertTemplateUsed(response,'User/signup.html')
    
    def test_using_function_login_views(self):
        response = resolve('/user/login/')
        self.assertEqual(response.func,login_view)

    def test_using_function_logout_view(self):
        response = resolve('/user/logout/')
        self.assertEqual(response.func,logout_view)

    def test_using_function_signup_view(self):
        response = resolve('/user/signup/')
        self.assertEqual(response.func,signup_view)

    def test_signup_add_user(self):
        response = self.client.post('/user/signup/',data={
            'username':"InsyallahPPWA",
            'password1':'mantap12345',
            'password2': 'mantap12345',
        })
        user = User.objects.all()
        self.assertEqual(len(user),2)
        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code,302)
        self.assertRedirects(expected_url='/',response=response)

    def test_signup_fail_password(self):
        response = self.client.post('/user/signup/',data={
            'username':"salahmen",
            'password1':'1234567',
            'password2': '1234567',
        })
        user = get_user_model().objects.all()

        self.assertNotIn('_auth_user_id',self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
        self.assertEqual(len(user),1)
        self.assertEqual(response.status_code,200)

    def test_login(self):
        response = self.client.post('/user/login/',data={
            'username':"samlekom",
            'password':'alfanganteng',
        })

        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/',response=response)

    def test_already_login(self):
        response = self.client.post('/user/login/',data={
            'username':"samlekom",
            'password':'alfanganteng',
        })

        response = self.client.get('/user/login/')

        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/',response=response)

        response = self.client.get('/user/signup/')

        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/',response=response)


    def test_login_fail_no_user(self):
        response = self.client.post('/user/login/',data={
            'username':'test',
            'password':'123412341234'
        })

        self.assertNotIn('_auth_user_id',self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 200)
