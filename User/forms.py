from django.contrib.auth import password_validation
from django.contrib.auth.forms import AuthenticationForm, UsernameField, UserCreationForm
from django.contrib.auth.models import User
from django.forms import CharField
from django.forms.widgets import PasswordInput, TextInput
from django.utils.translation import gettext, gettext_lazy as _



class UserCreateForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)

    password1 = CharField(
        label=_("Password"),
        strip=False,
        widget=PasswordInput(attrs={
            'class': 'form-control',
           'style': 'border: black solid px'
        }),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = CharField(
        label=_("Password confirmation"),
        widget=PasswordInput(attrs={
            'class': 'form-control',
            'style': 'border: black solid 1px'
        }),
        strip=False,
        help_text=_("Please enter the same password"),
    )

    class Meta:
        model = User
        fields = ("username",)
        field_classes = {'username': UsernameField}
        widgets = {
            'username': TextInput(attrs={
        'class': 'form-control',
        'style': 'border: black solid 1px'})
        }
        labels = {
            'username':_("Username")
        }
        
class UserLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)

    username = UsernameField(widget=TextInput(attrs={
        'class': 'form-control',
        'style': 'border: black solid 1px'}))
    password = CharField(
        label=_("Password"),
        strip=False,
        widget=PasswordInput(attrs={
            'class': 'form-control',
            'style': 'border: black solid 1px'
        }),
    )
from django.contrib.auth import password_validation
from django.contrib.auth.forms import AuthenticationForm, UsernameField, UserCreationForm
from django.contrib.auth.models import User
from django.forms import CharField
from django.forms.widgets import PasswordInput, TextInput
from django.utils.translation import gettext, gettext_lazy as _



class UserCreateForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)

    password1 = CharField(
        label=_("Password"),
        strip=False,
        widget=PasswordInput(attrs={
            'class': 'form-control',
           'style': 'border: #B09586 solid 1px'
        }),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = CharField(
        label=_("Password confirmation"),
        widget=PasswordInput(attrs={
            'class': 'form-control',
            'style': 'border: #B09586 solid 1px'
        }),
        strip=False,
        help_text=_("Please enter the same password"),
    )

    class Meta:
        model = User
        fields = ("username",)
        field_classes = {'username': UsernameField}
        widgets = {
            'username': TextInput(attrs={
        'class': 'form-control',
        'style': 'border: #B09586 solid 1px'})
        }
        labels = {
            'username':_("Username")
        }
        
class UserLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)

    username = UsernameField(widget=TextInput(attrs={
        'class': 'form-control',
        'style': 'border: #B09586 solid 1px'}))
    password = CharField(
        label=_("Password"),
        strip=False,
        widget=PasswordInput(attrs={
            'class': 'form-control',
            'style': 'border: #B09586 solid 1px'
        }),
    )
