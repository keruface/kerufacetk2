# Create your tests here.
from django.test import TestCase ,Client
from .models import Subscriber
from .forms import SubscriberForm
from django.test import TestCase, override_settings


class TestBarangViews (TestCase):

    def test_html(self):
        self.client = Client()
        # response = self.client.get('/subscriber/subscriber_create/')
        # self.assertEqual(response.status_code, 200)
        response1 = self.client.get('/subscriber/subscriber/')
        self.assertEqual(response1.status_code, 200)
        response2 = self.client.get('/subscriber/faq/')
        self.assertEqual(response2.status_code, 200)
        response3 = self.client.get("/subscriber/notAuth/")
        self.assertEqual(response3.status_code, 200)

    
    def test_using_index_template(self):
        self.client = Client()
        response1 = self.client.get('/subscriber/subscriber/')
        self.assertTemplateUsed(response1,"subscriber.html")
        # response2 = self.client.get('/subscriber/subscriber_create/')
        # self.assertTemplateUsed(response2, "koran.html")
        response3 = self.client.get('/subscriber/faq/')
        self.assertTemplateUsed(response3, "faq.html")
        response4 = self.client.get("/subscriber/notAuth/")
        self.assertTemplateUsed(response4, "not.html")

    def test_html_homepage1(self):
        self.client = Client()
        response1 = self.client.get('/subscriber/subscriber/')
        self.assertEqual(response1.status_code, 200)
    
    def test_contain_name (self):
        response = Client().get('/subscriber/subscriber/')
        html = response.content.decode('utf8')
        self.assertIn("Subscriber",html)

    def test_else_does_not_exist(self):
        response = Client().get('/rrrrrrrr/')
        self.assertEqual(response.status_code,404)
    
    def test_faq_has_title(self):
        response1 = Client().get('/subscriber/faq/')
        html_response1 = response1.content.decode('utf8')
        self.assertIn("FAQ", html_response1)


    
class TestBarangModel(TestCase):
        
    def test_buat_barang(self):
        subscriber_buat = Subscriber(
            name = "bro",
            email = "rani@error.com",
            
        )
        subscriber_buat.save()  
        self.assertEqual(Subscriber.objects.all().count(), 1)

    def test_buat_data(self):
        data = {
            'name' : "bro",
            'email' : "rani@error.com",
        }
        response = Client().post("subscriber/subscriber_create/", data)
        self.assertEqual(response.status_code, 404)


