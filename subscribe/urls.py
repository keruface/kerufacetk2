from django.urls import path
from . import views

app_name = 'subscribe'
urlpatterns = [
    path('subscriber_create/',views.subscriber_create,name='subscriber_create'),
    path('subscriber/',views.subscriber,name='subscriber'),
    path('faq/', views.faq, name='faq'),
    path('notAuth/', views.notAuth, name='notAuth'),
    path('daftar_subscriber/<str:value>' , views.get_JSON_daftar_subscriber , name = "daftar_subscriber")
]