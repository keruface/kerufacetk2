from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import Subscriber
from . import forms
from django.contrib.auth.decorators import login_required

# Create your views here.
def home(request):
    return render(request,'index.html',{})

@login_required
def subscriber_create(request):
        if request.method == 'POST':
            form = forms.SubscriberForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('subscribe:subscriber_create')

        form = forms.SubscriberForm()
        return render(request, 'koran.html', {'form': form})

def subscriber(request):
    subscribers = Subscriber.objects.order_by('id')
    return render(request, 'subscriber.html', {'subscribers': subscribers})

def notAuth(request):
    return render(request, 'not.html')

def faq(request):
    return render(request, 'faq.html')

def get_JSON_daftar_subscriber(request,value):
    result = list(Subscriber.objects.filter(name__icontains=value).values())
    return JsonResponse({"data": result})


