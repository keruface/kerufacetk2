// Change Header Colour 
$("#header-guest-1").hover(function() { 
    $('#header-guest-1').css("background-color", "#fcd6c0"); 
    }, function() { 
    $('#header-guest-1').css("background-color", "white"); 
}); 


// Carousel Implementation

const carouselSlide = $(".carousel-slide");
const carouselImages = $(".carousel-slide img");

const prevBtn = $("#prevBtn");
const nextBtn = $("#nextBtn");
let counter = 1;
const size = carouselImages[0].clientWidth;


$(carouselSlide).css("transform", "translateX("+(-size*counter)+"px)");


$(nextBtn).click(function() {
    if(counter>= carouselImages.length-1 ) return;
    $(carouselSlide).css("transition","transform 0.4s ease-in-out");
    counter++;
    $(carouselSlide).css("transform", "translateX("+(-size*counter)+"px)");
});

$(prevBtn).click(function() {
    if(counter <= 0 ) return;
    $(carouselSlide).css("transition","transform 0.4s ease-in-out");
    counter--;
    $(carouselSlide).css("transform", "translateX("+(-size*counter)+"px)");
});


(carouselSlide).bind('transitionend',function(){
    if(carouselImages[counter].id === "lastClone"){
        $(carouselSlide).css('transition',"none");
        counter = carouselImages.length-2;
        $(carouselSlide).css("transform", "translateX("+(-size*counter)+"px)");
    }
    if(carouselImages[counter].id === "firstClone"){
        $(carouselSlide).css('transition',"none");
        counter = carouselImages.length-counter;
        $(carouselSlide).css("transform", "translateX("+(-size*counter)+"px)");
    }
});


// Search Feature with AJAX GET

function cariBarang(){
    var search = $('#search').val()
    console.log(search)
    var data_barang = ""

    $.ajax({
        url: "/daftar_barang/"+search,
        dataType: "json",
        error: function(){
            alert("Something is Error , Please try again")
        }
        ,
        success: function(data){
            console.log(data)
            $('#barang').empty()
            if (data.data.length > 0){
                for( var i = 0 ; i<data.data.length ; i++){
                    data_barang +=  '<div class="col-md-6 col-xl-4 product-parent mt-4 mb-4">';
                    data_barang += '<div class="card" style="border: 3px solid #fcd6c0;">';
                    data_barang += '<img class ="image-product mb-4 card-img-top mt-4" src="/media/'+ data.data[i].picture + ' "alt="">';
                    data_barang += '<div class="card-body" style="border-top: 3px solid #fcd6c0;">';
                    data_barang += '<p class="brand-barang"> Brand : '+ data.data[i].brand_barang +  '</p>';
                    data_barang += '<p class="nama-barang">  Nama Barang :' + data.data[i].nama_barang + '</p>';
                    data_barang += '<p class="price-barang"> Harga :' + data.data[i].harga + ' </p>'
                    data_barang += '<form action="deskripsi'+ data.data[i].slug +'" enctype="multipart/form-data">'
                    data_barang += ' <button type="submit" class="btn btn-submit btn-lg mt-4">BUY</button>';
                    data_barang += '</form>';
                    data_barang += '</div>';
                    data_barang += '</div>';
                    data_barang += '</div>';
                }
            }
            else{
                data_barang += '<div class=display-2 mb-4 p-4> Barang tidak ditemukan </div>';               
            }
            console.log(data_barang)
            $("#barang").append(data_barang)
        },
    });
}