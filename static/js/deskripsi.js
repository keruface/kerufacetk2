$(function(){

    // jQuery methods go here...
    $('.toggle').click(function(e) {
        e.preventDefault();
    
      var $this = $(this);
    
      if ($this.next().hasClass('show')) {
          $this.next().removeClass('show');
          $this.next().slideUp(350);
      } else {
          $this.parent().parent().find('li .inner').removeClass('show');
          $this.parent().parent().find('li .inner').slideUp(350);
          $this.next().toggleClass('show');
          $this.next().slideToggle(350);
      }
    });
    $( ".inner-switch" ).on("click", function() {
        if( $( "body" ).hasClass( "dark" )) {
            $( "body" ).removeClass( "dark" );
            $( ".inner-switch" ).text( "OFF" );
            

          
        } else {
          $( "body" ).addClass( "dark" );
          $( ".inner-switch" ).text( "ON" );
          
        }
    });
});

var i = 0;
function divchange()
{
    var divtag = document.getElementById("div1");
    var txtcolor = ["#C4BDAC", "#E4DAC2", "#D3C4BE"];
    divtag.style.color = txtcolor[i];
    i = (i+1)%txtcolor.length;
}
setInterval(divchange, 350);

$(function(){
    $("img").hover(function(){
        $(this).width(1.25*$(this).width());
        $(this).height(1.25*$(this).height());
    }, function(){
        $(this).width(0.8*$(this).width());
        $(this).height(0.8*$(this).height());
    });
});

function caribarangterbeli(){
    var search = $('#search').val()
    console.log(search)
    var data_barang = ""

    $.ajax({
        url: "/deskripsi/caribarangterbeli/"+search,
        dataType: "json",
        error: function(){
            alert("Something is Error , Please try again")
        }
        ,
        success: function(data){
            console.log(data)
            $('#daftarbarangkebeli').empty()
            if (data.data.length > 0){
                for( var i = 0 ; i<data.data.length ; i++){

                    data_barang += '<div class="col-md-6">';
                    data_barang += '<img src=" '+ data.data[i].picture + '" alt="" class="image-product">';
                    data_barang += '</div>';
                    data_barang += '<div class="col-md-6">';
                    data_barang += '<p class="brand-barang"> '+ data.data[i].brand_barang +' </p>';
                    data_barang += '<p class="nama-barang"> '+  data.data[i].nama_barang +' </p>';
                    data_barang += '<p class="price-barang"> '+ data.data[i].harga +' </p>';
                    data_barang += '</div>';
                }
            }
            else{
                data_barang += '<div class=display-2 mb-4 p-4> the product is not found </div>';               
            }
            console.log(data_barang)
            $("#daftarbarangkebeli").append(data_barang)
        },
    });
}