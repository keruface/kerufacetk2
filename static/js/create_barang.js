var csrftoken = $("[name=csrfmiddlewaretoken]").val();

$("#create_barang").submit(function(e){
    e.preventDefault();
    $form = $(this)
    var formData = new FormData(this);
    $.ajax({
    type:"POST",
    dataType:'json',
    url: '/barang/',
    data: formData,
    success:function(data){
        console.log(data);
        $("#create_barang")[0].reset();
        alert("Barang telah berhasil dibuat");
        
    },
    error : function(xhr,errmsg,err,data) {
        console.log(data);
        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
    },
    cache: false,
    contentType: false,
    processData: false
    })
});