$(function(){

    // jQuery methods go here...
    $('.toggle').click(function(e) {
        e.preventDefault();
    
      var $this = $(this);
    
      if ($this.next().hasClass('show')) {
          $this.next().removeClass('show');
          $this.next().slideUp(350);
      } else {
          $this.parent().parent().find('li .inner').removeClass('show');
          $this.parent().parent().find('li .inner').slideUp(350);
          $this.next().toggleClass('show');
          $this.next().slideToggle(350);
      }
    });
    $( ".inner-switchs" ).on("click", function() {
        if( $( "body" ).hasClass( "dark" )) {
            $( "body" ).removeClass( "dark" );
            $( ".inner-switchs" ).text( "OFF" );
            

          
        } else {
          $( "body" ).addClass( "dark" );
          $( ".inner-switchs" ).text( "ON" );
          
        }
    });

});

var i = 0;
function divchange()
{
    var divtag = document.getElementById("div1");
    var txtcolor = ["#C4BDAC", "#E4DAC2", "#D3C4BE"];
    divtag.style.color = txtcolor[i];
    i = (i+1)%txtcolor.length;
}
setInterval(divchange, 350);

$(document).ready(function(){
    $("form").submit(function(){
        alert("Your review will be submitted");
    });
});

function carireview(){
    var search = $('#search').val()
    console.log(search)
    var data_barang = ""

    $.ajax({
        url: "/review/daftar_review/"+search,
        dataType: "json",
        error: function(){
            alert("Something is Error , Please try again")
        }
        ,
        success: function(data){
            console.log(data)
            $('#barang').empty()
            if (data.data.length > 0){
                for( var i = 0 ; i<data.data.length ; i++){

                    data_barang +=  '<div class="card mb-1 tor" style="width: 282px; margin-left: 3px; margin-right: 3px;">';
                    data_barang += '<div class="card-body">';
                    data_barang += '<h5 class="card-title">'+data.data[i].name+'</h5>';
                    data_barang += '<h6 class="card-subtitle mb-2 text-muted">testimoni</h6>';
                    data_barang += '<p class="card-text">'+data.data[i].fill+'</p>';
                    data_barang += '</div>';
                    data_barang += '</div>';
                }
            }
            else{
                data_barang += '<div class=display-2 mb-4 p-4> Submit review anda! </div>';               
            }
            console.log(data_barang)
            $("#barang").append(data_barang)
        },
    });
}