from django.shortcuts import render,redirect,HttpResponseRedirect,HttpResponse
from Barang.forms import BarangForm
from Barang.models import BarangModel
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required


@login_required(login_url='/user/login/')
def createBarang(request):
    if request.method == 'POST':
       form = BarangForm(request.POST, request.FILES)
       if form.is_valid():
            form.save()
            return JsonResponse({"success":True , "message" : "Barang berhasil ditambahkan"}, status=200)
       else:
            return JsonResponse({"success":False , "message" : "Something is Error"}, status= 400 )  
    else:
	    return render(request, "Barang/buatbarang.html", { "data": BarangForm()} )

def homepage(request):
    barang_all = BarangModel.objects.filter(terbeli=False)
    return render(request, "Barang/homepage.html", { "barang": barang_all,} )


def get_JSON_daftar_barang(request,value):
    result = list(BarangModel.objects.filter(nama_barang__icontains=value, terbeli=False).values())
    return JsonResponse({"data": result})

