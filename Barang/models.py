from django.db import models
from django.template.defaultfilters import slugify

class BarangModel(models.Model):
   Choice = {
       ("SkinCare", "Skincare"),
       ("Bedak", "Bedak"),
       ("Pelembab", "Pelembab"),
       ("Lipstik", "Lipstik"),
   }
   Choice_gender = {
       ("Perempuan", "Perempuan"),
       ("Laki-Laki", "Laki-Laki"),
       ("Both", "Both"),
   }
   brand_barang = models.CharField(max_length=250)
   nama_barang = models.CharField(max_length=500)
   harga = models.IntegerField(default=0)
   kategori_barang = models.CharField(choices=Choice, max_length=100)
   ukuran_barang = models.CharField(max_length=50)
   terbeli = models.BooleanField(default=False)
   gender = models.CharField( max_length=50 , choices = Choice_gender)
   picture = models.ImageField(upload_to = "media")
   slug = models.SlugField(unique=True)


   def _get_unique_slug(self): #For generating slug (url parameter for vendor)
      slug = slugify(self.nama_barang)
      unique_slug = slug
      num = 1
      while BarangModel.objects.filter(slug=unique_slug).exists():
         unique_slug = '{}-{}'.format(slug, num)
         num += 1
      return unique_slug

   def save(self, *args, **kwargs):
      if not self.slug:
         self.slug = self._get_unique_slug()
      super().save(*args,**kwargs)


