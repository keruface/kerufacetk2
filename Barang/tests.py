from django.test import TestCase ,Client
from Barang.models import BarangModel
from Barang.forms import BarangForm
import json
from django.urls import resolve
from . import views
from django.contrib.auth.models import User
import unittest

# Create your tests here.
class TestBarangViews (TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='alfan12345')
        login = self.client.login(username='testuser', password='alfan12345')

    def test_html(self):
        response = self.client.get('/barang/')
        self.assertEqual(response.status_code, 200)

    def test_html_formBarang(self):
        response = self.client.get('/barang/')
        self.assertTemplateUsed(response,"Barang/buatbarang.html")
    
    def test_contain_name (self):
        response = self.client.get('/barang/')
        html = response.content.decode('utf8')
        self.assertIn("Upload Product",html)

    def test_else_does_not_exist(self):
        response = Client().get('/antah-berantah/')
        self.assertEqual(response.status_code,404)

    def test_get_JSON_data_search(self):
        response = self.client.get("/daftar_barang/test")
        self.assertEqual(type(json.loads(response.content)),dict)
      
    def test_using_search_barang_functions(self):
        response = resolve("/daftar_barang/test")
        self.assertEqual(response.func,views.get_JSON_daftar_barang)

    
class TestBarangModel(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')

    def test_buat_barang(self):
        barang_buat = BarangModel(
            brand_barang = "Santuy",
            nama_barang = "bedakSans",
            harga = 30000,
            kategori_barang = "Bedak",
            ukuran_barang = "12 cm",
            terbeli = False,
            gender = "Laki-Laki",
        )
        barang_buat.save()  
        self.assertEqual(BarangModel.objects.all().count(), 1)

    def test_buat_data(self):
        from django.core.files.uploadedfile import SimpleUploadedFile
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif') 
        data = {
            'brand_barang' : "Santuy",
            'nama_barang' : "bedakSans",
            'harga' : 30000,
            'kategori_barang' : "Bedak",
            'ukuran_barang' : "12 cm",
            'terbeli' : False,
            'gender' : "Laki-Laki",
            'picture' : uploaded,
        }
        response = self.client.post("/barang/", data)
        self.assertEqual(response.status_code, 200)

    def buat_data_fail(self):
        data={
            'brand_barang' : "Santuy",
            'nama_barang' : "bedakSans",
            'harga' : 30000,
            'kategori_barang' : "Bedak",
            'ukuran_barang' : "12 cm",
        }
        response = self.client.post("/barang/", data)
        self.assertEqual(response.status_code, 400)
    



