from django import forms
from Barang.models import BarangModel
from django.utils.text import slugify

class BarangForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["kategori_barang"].widget.attrs.update({'class': 'form-control form-group row'})
        self.fields["gender"].widget.attrs.update({'class': 'form-control form-group row'})

    class Meta:
        model = BarangModel
        exclude = ('slug', 'base64_pic', 'terbeli')

        widgets= {
            'brand_barang': forms.TextInput(attrs={'class' : 'form-control form-group row' , 'placeholder' : 'Brand'}),
            'nama_barang' : forms.TextInput(attrs={'class': 'form-control form-group row', 'placeholder' : 'Name'}),
            'harga' : forms.NumberInput(attrs={'class': 'form-control form-group row' , 'placeholder' : 'Harga'}),
            'ukuran_barang' : forms.TextInput(attrs={'class': 'form-control form-group row' , 'placeholder' : 'Size'}),
            'deskripsi' : forms.Textarea(attrs={'class': 'form-control form-group row' , 'placeholder' : 'Gender'}),
        }

