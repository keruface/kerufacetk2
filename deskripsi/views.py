from django.shortcuts import render,redirect,HttpResponseRedirect,HttpResponse
from Barang.forms import BarangForm
from Barang.models import BarangModel
from deskripsi.models import BarangPembeli
from django.http import JsonResponse

from django.conf import settings
from django.contrib.auth.decorators import login_required

def detail_barang(request,slug_barang):    
    barang_detail = BarangModel.objects.get(slug = slug_barang)
    return render(request, 'deskripsi/deskripsi_barang.html',{
        "barang":barang_detail,
 })

@login_required
def beli_barang(request , slug_barang):
    barangnya_dibeli = BarangModel.objects.get(slug=slug_barang )
    barangnya_dibeli.terbeli = True
    barang_terbeli = BarangPembeli(barang = barangnya_dibeli)
    barang_terbeli.save()
    barangnya_dibeli.save()
    return redirect('/')

def barang_terbeli(request):
    barang_dibeli_pengunjung = BarangModel.objects.filter(terbeli=True)
    print(barang_dibeli_pengunjung)
    return render(request , 'deskripsi/barangterbeli.html' , {"barangkebeli" : barang_dibeli_pengunjung,} )

def get_JSON_daftar_barangterbeli(request,value):
    result = list(BarangModel.objects.filter(nama_barang__icontains=value).values())
    return JsonResponse({"data": result})