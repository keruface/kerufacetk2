from django.urls import path, include
from deskripsi.views import detail_barang, beli_barang, barang_terbeli, get_JSON_daftar_barangterbeli

app_name = 'deskripsi'

urlpatterns = [
    path("konfirmasi/<slug_barang>/" , beli_barang , name= 'barangbeli'),
    path('semuabarangdibeli/' , barang_terbeli , name='barangterbeli'),
    path("<slug_barang>/" , detail_barang , name= 'barangdetail'),
    path('caribarangterbeli/<str:value>/' , get_JSON_daftar_barangterbeli , name = "daftar_barangterbeli"),
]